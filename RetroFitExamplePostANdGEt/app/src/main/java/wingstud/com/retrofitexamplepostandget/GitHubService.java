package wingstud.com.retrofitexamplepostandget;

import com.google.gson.JsonObject;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;


/**
 * Created by wingstud on 23-12-2016.
 */
public interface GitHubService {
//    @GET("group/{id}/users")
//    Call<Loginjson> groupList(@Path("id") int groupId);

//    @FormUrlEncoded
//    @POST("login")
//    Call<Loginjson> getloginData(@Field("email")String email, @Field("password") String password);

//   @FormUrlEncoded
//   @Headers("Content-Type: application/json")
     @POST("api/VoidOrderItem")
     Call<VoidItem> getVoidData(@Body RequestBody params);

//  RR
    @GET("GetCartCount.php?user_id=95")
    Call<JsonObject> groupList();

}
