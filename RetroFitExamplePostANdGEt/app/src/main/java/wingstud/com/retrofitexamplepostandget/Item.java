
package wingstud.com.retrofitexamplepostandget;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Item {
    public Item(String itemid, String voidreason, String voidid, String trorderid, String transorderId, String preparestatus) {
        this.itemid = itemid;
        this.voidreason = voidreason;
        this.voidid = voidid;
        this.trorderid = trorderid;
        this.transorderId = transorderId;
        this.preparestatus = preparestatus;
    }


    private String itemid;

    private String voidreason;

    private String voidid;

    private String trorderid;

    private String transorderId;


    private String preparestatus;

    public String getItemid() {
        return itemid;
    }

    public void setItemid(String itemid) {
        this.itemid = itemid;
    }

    public String getVoidreason() {
        return voidreason;
    }

    public void setVoidreason(String voidreason) {
        this.voidreason = voidreason;
    }

    public String getVoidid() {
        return voidid;
    }

    public void setVoidid(String voidid) {
        this.voidid = voidid;
    }

    public String getTrorderid() {
        return trorderid;
    }

    public void setTrorderid(String trorderid) {
        this.trorderid = trorderid;
    }

    public String getTransorderId() {
        return transorderId;
    }

    public void setTransorderId(String transorderId) {
        this.transorderId = transorderId;
    }

    public String getPreparestatus() {
        return preparestatus;
    }

    public void setPreparestatus(String preparestatus) {
        this.preparestatus = preparestatus;
    }

}
