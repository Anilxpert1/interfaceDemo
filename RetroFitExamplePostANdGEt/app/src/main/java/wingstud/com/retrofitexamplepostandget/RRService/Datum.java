
package wingstud.com.retrofitexamplepostandget.RRService;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Datum {

    @SerializedName("cat_id")
    @Expose
    private String catId;
    @SerializedName("cat_name")
    @Expose
    private String catName;
    @SerializedName("child_cats")
    @Expose
    private List<ChildCat> childCats = null;

    public String getCatId() {
        return catId;
    }

    public void setCatId(String catId) {
        this.catId = catId;
    }

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }

    public List<ChildCat> getChildCats() {
        return childCats;
    }

    public void setChildCats(List<ChildCat> childCats) {
        this.childCats = childCats;
    }

}
