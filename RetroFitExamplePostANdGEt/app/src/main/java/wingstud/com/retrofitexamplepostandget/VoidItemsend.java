
package wingstud.com.retrofitexamplepostandget;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VoidItemsend {
    public VoidItemsend(String dbname, String ipaddress, String username, String password, String islocal, String hotelid, String departmentid, String outletid, String loginid, String financialyearid, List<Item> items) {
        this.dbname = dbname;
        this.ipaddress = ipaddress;
        this.username = username;
        this.password = password;
        this.islocal = islocal;
        this.hotelid = hotelid;
        this.departmentid = departmentid;
        this.outletid = outletid;
        this.loginid = loginid;
        this.financialyearid = financialyearid;
        this.items = items;
    }

    private String dbname;

    private String ipaddress;

    private String username;

    private String password;

    private String islocal;

    private String hotelid;

    private String departmentid;

    private String outletid;

    private String loginid;

    private String financialyearid;

    private List<Item> items = null;

    public String getDbname() {
        return dbname;
    }

    public void setDbname(String dbname) {
        this.dbname = dbname;
    }

    public String getIpaddress() {
        return ipaddress;
    }

    public void setIpaddress(String ipaddress) {
        this.ipaddress = ipaddress;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getIslocal() {
        return islocal;
    }

    public void setIslocal(String islocal) {
        this.islocal = islocal;
    }

    public String getHotelid() {
        return hotelid;
    }

    public void setHotelid(String hotelid) {
        this.hotelid = hotelid;
    }

    public String getDepartmentid() {
        return departmentid;
    }

    public void setDepartmentid(String departmentid) {
        this.departmentid = departmentid;
    }

    public String getOutletid() {
        return outletid;
    }

    public void setOutletid(String outletid) {
        this.outletid = outletid;
    }

    public String getLoginid() {
        return loginid;
    }

    public void setLoginid(String loginid) {
        this.loginid = loginid;
    }

    public String getFinancialyearid() {
        return financialyearid;
    }

    public void setFinancialyearid(String financialyearid) {
        this.financialyearid = financialyearid;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

}
