
package wingstud.com.retrofitexamplepostandget.RRService;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ChildCat_ {

    @SerializedName("cat_id")
    @Expose
    private String catId;
    @SerializedName("cat_name")
    @Expose
    private String catName;
    @SerializedName("child_cats")
    @Expose
    private Object childCats;

    public String getCatId() {
        return catId;
    }

    public void setCatId(String catId) {
        this.catId = catId;
    }

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }

    public Object getChildCats() {
        return childCats;
    }

    public void setChildCats(Object childCats) {
        this.childCats = childCats;
    }

}
