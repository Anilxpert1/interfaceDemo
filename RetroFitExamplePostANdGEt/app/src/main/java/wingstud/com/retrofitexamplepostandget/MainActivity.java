package wingstud.com.retrofitexamplepostandget;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {
    ProgressDialog progressDialog;
    VoidItemsend voidItemsend;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait...");
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
                progressDialog.show();
                retro();

            }
        });

    }

    //        public void retro() {
//        Retrofit retrofit = new Retrofit.Builder().baseUrl("http://192.168.1.105/vehicle_tracking/api/").addConverterFactory(GsonConverterFactory.create()).build();
//        GitHubService gitHubService = retrofit.create(GitHubService.class);
//        Call<Loginjson> loginjsonCall = gitHubService.getloginData("wingstudinfotech@gmail.com", "123456");
//        loginjsonCall.enqueue(new Callback<Loginjson>() {
//            @Override
//            public void onResponse(Call<Loginjson> call, Response<Loginjson> response) {
//                Loginjson loginjson = response.body();
//                String s = loginjson.getStatus();
//                progressDialog.dismiss();
//                Toast.makeText(getApplicationContext(), s, Toast.LENGTH_SHORT).show();
//            }
//
//            @Override
//            public void onFailure(Call<Loginjson> call, Throwable t) {
//                Toast.makeText(getApplicationContext(), "Fail", Toast.LENGTH_SHORT).show();
//                progressDialog.dismiss();
//            }
//        });
//    }

    public void retro() {
        Retrofit retrofit = new Retrofit.Builder().baseUrl("https://theultimatelawyer.in/appApis/").addConverterFactory(GsonConverterFactory.create()).build();
        GitHubService gitHubService = retrofit.create(GitHubService.class);
        Call<JsonObject> loginjsonCall = gitHubService.groupList();
        loginjsonCall.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                JsonObject loginjson = response.body();

                progressDialog.dismiss();
                Toast.makeText(getApplicationContext(), "Success" + loginjson.toString(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Fail", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }
}
